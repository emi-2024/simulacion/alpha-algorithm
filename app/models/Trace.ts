export class Trace {
    public frequency: number;
    public value: string[];
    public readonly startActivity: string;
    public readonly endActivity: string;

    constructor(activities: string[], frequency?: number) {
        // this.value = this.asignValue(activities);
        this.value = activities;
        this.frequency = frequency ? frequency : 1;
        this.startActivity = this.getStartActivity();
        this.endActivity = this.getEndActivity();
    }

    public toString(): string {
        return `<${this.value.join()}>`;
    }

    public addActivity(activity: string) {
        this.value.push(activity);
    }

    private getStartActivity(): string {
        return this.value[0];
    }

    private getEndActivity(): string {
        return this.value[this.value.length - 1];
    }



    // private areActivitiesUnique(activities: Activity[]): boolean {
    //     return activities.filter((ac, ind, arr) => arr.indexOf(ac) !== ind).length < 0;
    // }


    // private asignValue(activities: Activity[]) {
    //     return this.areActivitiesUnique(activities) ? activities : [];
    // }
}