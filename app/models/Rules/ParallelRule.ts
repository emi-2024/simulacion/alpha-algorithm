import { RuleType } from "./RuleType";

export class ParallelRule implements RuleType {
    public processSource(direct: [string, string][]): [string, string][] {

        const seen = new Set<string>();

        const parallel = direct.reduce<[string, string][]>((parallelArr, tuple) => {
            const key = tuple.join('');
            const reversedTuple = [...tuple].reverse();

            if (!seen.has(key)) {
                seen.add(key);
                if (seen.has(reversedTuple.join(''))) {
                    parallelArr.push([reversedTuple[0], reversedTuple[1]]);
                    parallelArr.push(tuple);
                }
            }

            return parallelArr;

        }, [])

        return parallel;
    }

    public symbol(): string {
        return '||';
    }

}

