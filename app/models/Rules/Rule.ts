import { FoodPrintEntity } from "../FoodPrintEntity.js";
import { RuleType } from "./RuleType.js";

export class Rule extends FoodPrintEntity<[string, string][], "waiting" | "done">{
    public getValue(): [string, string][] {
        return this.value;
    }

    public symbol: string;
    private _ruleType: RuleType;

    constructor(ruleType: RuleType) {
        super([], 'waiting');
        this._ruleType = ruleType;
        this.symbol = this._ruleType.symbol();
    }

    public applyRule(direct: [string, string][]): void; // causal parallel
    public applyRule(log: string[][]): void;  // direct
    public applyRule(direct: [string, string][], combinations: [string, string][]): void; // decisional
    public applyRule(source: [string, string][] | string[][], combinations?: [string, string][]): void {
        this.value = this._ruleType.processSource(source, combinations);
        this.status = 'done';
    }



    public isDone(): boolean {
        return this.status === 'done';
    }

    public toString(): string {
        return `${this.symbol}`
    }


}