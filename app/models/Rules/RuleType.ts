export abstract class RuleType {
    public abstract processSource(source: string[][] | [string, string][], combinations?: [string, string][]): [string, string][];

    public abstract symbol(): string;
}