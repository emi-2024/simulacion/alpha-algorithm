import { RuleType } from "./RuleType";

export class DecisionalRule implements RuleType {

    public processSource(direct: [string, string][], combinations: [string, string][]): [string, string][] {
        const filterSet = this.getFilteringSet(direct);
        const decisional = combinations.filter(tuple => !filterSet.has(tuple.join('')));
        return decisional;
    }

    private getFilteringSet(source: [string, string][]): Set<string> {
        const reversedTuples = source.map(tuple => [tuple[1], tuple[0]]) as [string, string][];
        const filteringSet = new Set<string>(source.concat(reversedTuples).map(tuple => tuple.join('')));
        return filteringSet;
    }

    public symbol(): string {
        return '#';
    }

}