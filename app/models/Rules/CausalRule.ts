import { RuleType } from "./RuleType.js";
import { ParallelRule } from "./ParallelRule.js";

export class CausalRule implements RuleType {

    public processSource(direct: [string, string][]): [string, string][] {
        const parallels = new ParallelRule().processSource(direct);
        const seenParallels = new Set<string>(parallels.map(item => item.join('')));

        const causal = direct.reduce<[string, string][]>((causalArr, tuple) => {
            const key = tuple.join('');

            if (!seenParallels.has(key)) {
                causalArr.push(tuple);
            }

            return causalArr;
        }, [])

        return causal;
    }





    public symbol(): string {
        return '->';
    }
}