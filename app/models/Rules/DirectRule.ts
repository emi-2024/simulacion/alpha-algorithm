import { RuleType } from "./RuleType";

export class DirectRule implements RuleType {

    public processSource(log: string[][]): [string, string][] {
        const tupledLog = this.logToTupleArr(log);
        const direct = this.getUniqueTuples(tupledLog);
        return direct;

    }

    private logToTupleArr(log: string[][]): [string, string][] {
        const tupleArr: [string, string][] = [];
        log.forEach((trace) => {
            for (let i = 1; i < trace.length; i++) {
                const currentTuple: [string, string] = [trace[i - 1], trace[i]]
                tupleArr.push(currentTuple);
            }
        })
        return tupleArr;
    }

    private getUniqueTuples(source: [string, string][]): [string, string][] {
        const seenTuples = new Set<string>();

        const uniqueTupleArr = source.reduce<[string, string][]>((uniqueTuples, currTuple) => {
            const key = currTuple.join('');
            if (!seenTuples.has(key)) {
                seenTuples.add(key);
                uniqueTuples.push(currTuple);
            }

            return uniqueTuples;
        }, [])

        return uniqueTupleArr;
    }


    public symbol(): string {
        return '>';
    }

}