import { Log } from "./Log.js";
import { CausalRule } from "./Rules/CausalRule.js";
import { DecisionalRule } from "./Rules/DecisionalRule.js";
import { DirectRule } from "./Rules/DirectRule.js";
import { ParallelRule } from "./Rules/ParallelRule.js";
import { Rule } from "./Rules/Rule.js";
import { Trace } from "./Trace.js";

interface Rules {
    Direct: Rule,
    Causal: Rule,
    Parallel: Rule,
    Decisional: Rule,
}
interface OutputRules {
    Direct: [string, string][],
    Causal: [string, string][],
    Parallel: [string, string][],
    Decisional: [string, string][],
}

export class FootPrint {
    private log: Log;
    private rules: Rules;

    constructor(log: string[][]) {
        this.log = this.getLogFromInput(log);
        this.rules = {
            Causal: new Rule(new CausalRule()),
            Parallel: new Rule(new ParallelRule()),
            Direct: new Rule(new DirectRule()),
            Decisional: new Rule(new DecisionalRule()),
        };
    }

    private getLogFromInput(log: string[][]): Log {
        const outLog = new Log(
            log.map(trace => new Trace(trace))
        )
        return outLog;
    }

    public applyRelationshipOrder(): void {
        console.time('Applying relationship order');
        this.rules.Direct.applyRule(this.log.getValue());
        this.rules.Causal.applyRule(this.rules.Direct.getValue());
        this.rules.Parallel.applyRule(this.rules.Direct.getValue());
        this.rules.Decisional.applyRule(this.rules.Direct.getValue(), this.log.getActivityPairCombinations());
        console.timeEnd('Applying relationship order');
    }

    public getFootprintTable(output: 'matrix' | 'object'): string[][] | {} {
        throw new Error('Get footprint table not implemented');
    }

    public Rules(): OutputRules {
        throw new Error('Rules not implemented');
    }
}