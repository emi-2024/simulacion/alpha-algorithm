import { FoodPrintEntity } from "./FoodPrintEntity.js";
import { Trace } from "./Trace";

export class Log extends FoodPrintEntity<Trace[], 'in progress' | 'completed'>{
    public id: number;
    public readonly startActivities: Set<string>;
    public readonly endActivities: Set<string>;
    public readonly activitySet: Set<string>;


    constructor(value: Trace[]) {
        super(value, 'in progress');
        this.id = 1;
        this.startActivities = this.setStartActivities();
        this.endActivities = this.setEndActivities();
        this.activitySet = this.setActivitySet();
    }

    public toString(): string {
        return `[${this.value.toString()}]`;
    }

    public updateValue(value: Trace[]): void {
        this.value = value;
        this.setActivitySet();
        this.setEndActivities();
        this.setStartActivities();
    }

    private setActivitySet(): Set<string> {
        throw new Error('Set activity set not implemented') //implement
    }

    private setStartActivities(): Set<string> {
        throw new Error('Set start activities not implemented') //implement
    }

    private setEndActivities(): Set<string> {
        throw new Error('Set end activities not implemented') //implement
    }

    public getActivityPairCombinations(): [string, string][] {
        throw new Error('get activity pair combination not implemented') //implement
    }


    public getValue(): string[][] {
        return this.value.map(trace => trace.value);
    }
}