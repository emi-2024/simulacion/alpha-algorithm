export class Activity {
    public name: string;
    public meaning: string;

    constructor(name: string, meaning: string) {
        this.name = name;
        this.meaning = meaning;
    }

    public toString(): string {
        return this.name;
    }

    private isNameEqual(name: string) {
        return this.name === name;
    }

    private isMeaningEqual(meaning: string) {
        return this.meaning.toLowerCase() == meaning.toLowerCase();
    }

    public isEqual(activity: Activity) {
        return this.isNameEqual(activity.name) && this.isMeaningEqual(activity.meaning);
    }
}