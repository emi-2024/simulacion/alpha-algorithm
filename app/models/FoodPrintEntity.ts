export abstract class FoodPrintEntity<t_value, t_status>{
    protected value: t_value;
    public status: t_status;

    public constructor(value: t_value, status: t_status) {
        this.value = value;
        this.status = status;
    }
    public abstract toString(): string;

    public abstract getValue(): unknown;
    // public abstract getValue(): t_value;
    // public abstract setValue(value: t_value): void;
}