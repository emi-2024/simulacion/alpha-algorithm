import { assert } from "chai";
import { ParallelRule } from "../models/Rules/ParallelRule.js";
import { Rule } from "../models/Rules/Rule.js"
import { testCase1, testCase2, testCase3 } from "./RulesTestCases.js";

describe('Testing Parallel rule', () => {
    let parallel: Rule;

    it('Correct symbol assignation', () => {
        parallel = new Rule(new ParallelRule());
        assert.strictEqual(parallel.symbol, '||');
    })

    it('Correct value obtained 1', () => {
        parallel = new Rule(new ParallelRule());
        parallel.applyRule(testCase1.direct);

        assert.deepEqual(parallel.getValue(), testCase1.parallel);
    })

    it('Correct value obtained 2', () => {
        parallel = new Rule(new ParallelRule());
        parallel.applyRule(testCase2.direct);

        assert.deepEqual(parallel.getValue(), testCase2.parallel);
    })

    it('Correct value obtained 3', () => {
        parallel = new Rule(new ParallelRule());
        parallel.applyRule(testCase3.direct);

        assert.deepEqual(parallel.getValue(), testCase3.parallel);
    })

    it('Correct status update after the rule application', () => {
        parallel = new Rule(new ParallelRule());
        parallel.applyRule(testCase1.direct);

        assert.strictEqual(parallel.status, 'done');
    })
})