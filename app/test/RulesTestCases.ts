
// graficar con mermaid
interface TestCase {
    log: string[][],
    direct: [string, string][],
    causal: [string, string][],
    parallel: [string, string][],
    decisional: [string, string][],
    combinations: [string, string][],
}

export const testCase1: TestCase = {
    log: [
        ['a', 'b', 'c', 'd', 'f'],
        ['a', 'c', 'b', 'd', 'e'],
        ['a', 'c', 'b', 'd', 'f'],
        ['a', 'b', 'c', 'd', 'e'],
    ],
    direct: [
        ['a', 'b'], ['b', 'c'], ['c', 'd'], ['d', 'f'],
        ['a', 'c'], ['c', 'b'], ['b', 'd'], ['d', 'e'],
    ],
    causal: [
        ['a', 'b'], ['c', 'd'], ['d', 'f'],
        ['a', 'c'], ['b', 'd'], ['d', 'e'],
    ],
    parallel: [
        ['b', 'c'], ['c', 'b'],
    ],
    decisional: [
        ["a", "a"], ["a", "d"], ["a", "e"], ["a", "f"],
        ["b", "b"], ["b", "e"], ["b", "f"], ["c", "c"],
        ["c", "e"], ["c", "f"], ["d", "a"], ["d", "d"],
        ["e", "a"], ["e", "b"], ["e", "c"], ["e", "e"],
        ["e", "f"], ["f", "a"], ["f", "b"], ["f", "c"],
        ["f", "e"], ["f", "f"]
    ],

    combinations: [
        ['a', 'a'], ['a', 'b'], ['a', 'c'], ['a', 'd'], ['a', 'e'], ['a', 'f'],
        ['b', 'a'], ['b', 'b'], ['b', 'c'], ['b', 'd'], ['b', 'e'], ['b', 'f'],
        ['c', 'a'], ['c', 'b'], ['c', 'c'], ['c', 'd'], ['c', 'e'], ['c', 'f'],
        ['d', 'a'], ['d', 'b'], ['d', 'c'], ['d', 'd'], ['d', 'e'], ['d', 'f'],
        ['e', 'a'], ['e', 'b'], ['e', 'c'], ['e', 'd'], ['e', 'e'], ['e', 'f'],
        ['f', 'a'], ['f', 'b'], ['f', 'c'], ['f', 'd'], ['f', 'e'], ['f', 'f'],
    ]
}

export const testCase2: TestCase = {
    log: [
        ['a', 'b', 'c', 'd'],
        ['a', 'c', 'b', 'd'],
        ['a', 'e', 'd'],
    ],
    direct: [
        ['a', 'b'], ['b', 'c'], ['c', 'd'], ['a', 'c'],
        ['c', 'b'], ['b', 'd'], ['a', 'e'], ['e', 'd'],
    ],
    causal: [
        ['a', 'b'], ['c', 'd'], ['a', 'c'],
        ['b', 'd'], ['a', 'e'], ['e', 'd'],
    ],
    parallel: [
        ['b', 'c'], ['c', 'b'],
    ],
    decisional: [
        ['a', 'a'], ['a', 'd'], ['b', 'b'], ['b', 'e'],
        ['c', 'c'], ['c', 'e'], ['d', 'a'], ['d', 'd'],
        ['e', 'b'], ['e', 'c'], ['e', 'e']
    ],
    combinations: [
        ['a', 'a'], ['a', 'b'], ['a', 'c'], ['a', 'd'], ['a', 'e'],
        ['b', 'a'], ['b', 'b'], ['b', 'c'], ['b', 'd'], ['b', 'e'],
        ['c', 'a'], ['c', 'b'], ['c', 'c'], ['c', 'd'], ['c', 'e'],
        ['d', 'a'], ['d', 'b'], ['d', 'c'], ['d', 'd'], ['d', 'e'],
        ['e', 'a'], ['e', 'b'], ['e', 'c'], ['e', 'd'], ['e', 'e'],
    ]
}

export const testCase3: TestCase = {
    log: [
        ['a', 'b', 'd'],
        ['a', 'c', 'd'],
    ],
    direct: [
        ['a', 'b'], ['b', 'd'],
        ['a', 'c'], ['c', 'd']
    ],
    causal: [
        ['a', 'b'], ['b', 'd'],
        ['a', 'c'], ['c', 'd']
    ],
    parallel: [],
    decisional: [
        ['a', 'a'], ['a', 'd'],
        ['b', 'b'], ['b', 'c'],
        ['c', 'b'], ['c', 'c'],
        ['d', 'a'], ['d', 'd'],
    ],
    combinations: [
        ['a', 'a'], ['a', 'b'], ['a', 'c'], ['a', 'd'],
        ['b', 'a'], ['b', 'b'], ['b', 'c'], ['b', 'd'],
        ['c', 'a'], ['c', 'b'], ['c', 'c'], ['c', 'd'],
        ['d', 'a'], ['d', 'b'], ['d', 'c'], ['d', 'd'],
    ]

}

