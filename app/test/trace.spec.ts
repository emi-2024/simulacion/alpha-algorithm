import { Trace } from '../models/Trace.js'
import { Activity } from "../models/Activity.js"
import { assert } from 'chai'

describe('Testing trace class', () => {

    it('Parse to string', () => {
        assert.strictEqual(new Trace(['a', 'b', 'c', 'd']).toString(), '<a,b,c,d>');
    })

    it('Getting correct start activity 1')
    it('Getting correct end activity 1')

    it('Getting correct start activity 2')
    it('Getting correct end activity 2')

    it('Getting correct start activity 3')
    it('Getting correct end activity 3')


})