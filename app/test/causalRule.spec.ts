import { assert } from "chai";
import { CausalRule } from "../models/Rules/CausalRule.js"
import { Rule } from "../models/Rules/Rule.js"
import { testCase1, testCase2, testCase3 } from "./RulesTestCases.js";

describe('Testing Causal rule', () => {
    let causal: Rule;

    it('Correct symbol assignation', () => {
        causal = new Rule(new CausalRule());
        assert.strictEqual(causal.symbol, '->');
    })

    it('Correct value obtained 1', () => {
        causal = new Rule(new CausalRule());
        causal.applyRule(testCase1.direct);

        assert.deepEqual(causal.getValue(), testCase1.causal);
    })

    it('Correct value obtained 2', () => {
        causal = new Rule(new CausalRule());
        causal.applyRule(testCase2.direct);

        assert.deepEqual(causal.getValue(), testCase2.causal)
    })

    it('Correct value obtained 3', () => {
        causal = new Rule(new CausalRule());
        causal.applyRule(testCase3.direct);

        assert.deepEqual(causal.getValue(), testCase3.causal)
    })

    it('Correct status update after the rule application', () => {
        causal = new Rule(new CausalRule());
        causal.applyRule(testCase2.direct);

        assert.strictEqual(causal.status, 'done');
    })
})