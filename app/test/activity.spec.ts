import { assert } from "chai"
import { Activity } from "../models/Activity.js"


describe('Testing activity class', () => {

    describe('Object comparison', () => {
        it('Same case, same names and meanings', () => {
            assert.equal(new Activity('a', 'meaning 1').isEqual(new Activity('a', 'meaning 1')), true);
            assert.equal(new Activity('B', 'MEANING 2').isEqual(new Activity('B', 'MEANING 2')), true);
        })

        it('Different case on name', () => {
            assert.equal(new Activity('A', 'meaning 1').isEqual(new Activity('a', 'meaning 1')), false);
        })

        it('Different case on meaning', () => {
            assert.equal(new Activity('a', 'meAnIng 1').isEqual(new Activity('a', 'meaning 1')), true);
            assert.equal(new Activity('b', 'interestinG MeaNiNg 1').isEqual(new Activity('b', 'InteResTing mEaninG 1')), true);
        })

        it('Different values', () => {
            assert.equal(new Activity('A', 'meaning 1').isEqual(new Activity('b', 'meaning 2')), false);
            assert.equal(new Activity('c', 'meaning 3').isEqual(new Activity('b', 'meaning 2')), false);
        })
    })

})