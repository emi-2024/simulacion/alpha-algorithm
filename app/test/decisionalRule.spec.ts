import { assert } from "chai";
import { DecisionalRule } from "../models/Rules/DecisionalRule.js";
import { Rule } from "../models/Rules/Rule.js";
import { testCase1, testCase2, testCase3 } from "./RulesTestCases.js";

describe('Testing Decisional rule', () => {
    let decisional: Rule;

    it('Correct symbol assignation', () => {
        decisional = new Rule(new DecisionalRule());
        assert.strictEqual(decisional.symbol, '#');
    })

    it('Correct value obtained 1', () => {
        decisional = new Rule(new DecisionalRule());
        decisional.applyRule(testCase1.direct, testCase1.combinations);

        assert.deepEqual(decisional.getValue(), testCase1.decisional);
    })

    it('Correct value obtained 2', () => {
        decisional = new Rule(new DecisionalRule());
        decisional.applyRule(testCase2.direct, testCase2.combinations);

        assert.deepEqual(decisional.getValue(), testCase2.decisional);
    })

    it('Correct value obtained 3', () => {
        decisional = new Rule(new DecisionalRule());
        decisional.applyRule(testCase3.direct, testCase3.combinations);

        assert.deepEqual(decisional.getValue(), testCase3.decisional);
    })

    it('Correct status update after the rule application', () => {
        decisional = new Rule(new DecisionalRule());
        decisional.applyRule(testCase3.direct, testCase3.combinations);

        assert.strictEqual(decisional.status, 'done');
    })
})