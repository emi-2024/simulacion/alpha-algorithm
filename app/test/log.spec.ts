import { assert } from "chai"
import { Log } from "../models/Log.js"
import { Trace } from "../models/Trace.js"

let trace1 = new Trace(['a', 'b', 'c']);
let trace2 = new Trace(['a', 'd', 'c']);

describe('Testing Log class:', () => {
    describe('Creating a Log correctly', () => {
        it('Correct initialization of activity set', () => {
            assert.deepEqual(new Log([
                new Trace(['a', 'b', 'c', 'e']),
                new Trace(['a', 'b', 'd', 'e']),
            ]).activitySet, ['a', 'b', 'c', 'd', 'e'])
        });

        it('Correct initialization of starting activities set', () => {
            assert.deepEqual(new Log([
                new Trace(['a', 'b', 'b', 'd']),
                new Trace(['a', 'c', 'b', 'd']),
                new Trace(['a', 'b', 'e', 'd']),
            ]).startActivities, ['a']);

            assert.deepEqual(new Log([
                new Trace(['a', 'b']),
                new Trace(['b', 'c']),
                new Trace(['c', 'b']),
                new Trace(['c', 'e']),
                new Trace(['d', 'b']),
            ]).startActivities, ['a', 'b', 'c', 'd']);
        });

        it('Correct initialization of ending activities set', () => {
            assert.deepEqual(new Log([
                new Trace(['a', 'b', 'b', 'd']),
                new Trace(['a', 'c', 'b', 'd']),
                new Trace(['a', 'b', 'e', 'd']),
            ]).endActivities, ['d']);

            assert.deepEqual(new Log([
                new Trace(['d', 'b', 'c']),
                new Trace(['a', 'b', 'e']),
                new Trace(['a', 'b', 'd']),
                new Trace(['a', 'b', 'a']),
                new Trace(['a', 'b', 'c']),
            ]).endActivities, ['a', 'c', 'd', 'e']);
        });
    });

    it('Parse to string', () => {
        assert.strictEqual(new Log([trace1, trace2]).toString(), '[<a,b,c>,<a,d,c>]');
    });

    it('Getting correct starting set 1');
    it('Getting correct ending set 1');
    it('Getting correct activity set 1');

    it('Getting correct starting set 2');
    it('Getting correct ending set 2');
    it('Getting correct activity set 2');

    it('Getting correct starting set 3');
    it('Getting correct ending set 3');
    it('Getting correct activity set 3');
})