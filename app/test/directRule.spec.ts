import { assert } from "chai";
import { DirectRule } from "../models/Rules/DirectRule.js"
import { Rule } from "../models/Rules/Rule.js"
import { testCase1, testCase2, testCase3 } from "./RulesTestCases.js";

describe('Testing Direct rule', () => {
    let direct: Rule;

    it('Correct symbol assignation', () => {
        direct = new Rule(new DirectRule());
        assert.strictEqual(direct.symbol, '>');
    })

    it('Correct value obtained 1', () => {
        direct = new Rule(new DirectRule());
        direct.applyRule(testCase1.log);

        assert.deepEqual(direct.getValue(), testCase1.direct);
    })

    it('Correct value obtained 2', () => {
        direct = new Rule(new DirectRule());
        direct.applyRule(testCase2.log);
        assert.deepEqual(direct.getValue(), testCase2.direct);
    })

    it('Correct value obtained 3', () => {
        direct = new Rule(new DirectRule());
        direct.applyRule(testCase3.log);
        assert.deepEqual(direct.getValue(), testCase3.direct);
    })

    it('Correct status update after the rule application', () => {
        direct = new Rule(new DirectRule());
        direct.applyRule(testCase3.log);

        assert.strictEqual(direct.status, 'done');
    })

})